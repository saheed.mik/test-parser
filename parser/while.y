%baseclass-preinclude <iostream>

%lsp-needed

%token TOKEN_PROGRAM
%token TOKEN_BEGIN
%token TOKEN_END
%token TOKEN_INTEGER 
%token TOKEN_BOOLEAN
%token TOKEN_SKIP
%token TOKEN_IF
%token TOKEN_THEN
%token TOKEN_ELSE
%token TOKEN_ENDIF
%token TOKEN_WHILE
%token TOKEN_DO
%token TOKEN_DONE
%token TOKEN_READ
%token TOKEN_WRITE
%token TOKEN_SEMICOLON
%token TOKEN_ASSIGN
%token TOKEN_OPEN
%token TOKEN_CLOSE
%token TOKEN_NUM
%token TOKEN_TRUE
%token TOKEN_FALSE
%token TOKEN_ID

%left TOKEN_OR
%left TOKEN_AND
%left TOKEN_NOT
%left TOKEN_EQ
%left TOKEN_LESS TOKEN_GR
%left TOKEN_ADD TOKEN_SUB
%left TOKEN_MUL TOKEN_DIV TOKEN_MOD

%%

start:
    TOKEN_PROGRAM TOKEN_ID declarations TOKEN_BEGIN instructions TOKEN_END
    {
        std::cout << "start -> TOKEN_PROGRAM TOKEN_ID declarations TOKEN_BEGIN instructions TOKEN_END" << std::endl;
    }
;

declarations:
    // empty
    {
        std::cout << "declarations -> epsilon" << std::endl;
    }
|
    declaration declarations
    {
        std::cout << "declarations -> declaration declarations" << std::endl;
    }
;

declaration:
    TOKEN_INTEGER TOKEN_ID TOKEN_SEMICOLON
    {
        std::cout << "declaration -> TOKEN_INTEGER TOKEN_ID TOKEN_SEMICOLON" << std::endl;
    }
|
    TOKEN_BOOLEAN TOKEN_ID TOKEN_SEMICOLON
    {
        std::cout << "declaration -> TOKEN_BOOLEAN TOKEN_ID TOKEN_SEMICOLON" << std::endl;
    }
;

instructions:
    instruction
    {
        std::cout << "instructions -> instruction" << std::endl;
    }
|
    instruction instructions
    {
        std::cout << "instructions -> instruction instructions" << std::endl;
    }
;

instruction:
    TOKEN_SKIP TOKEN_SEMICOLON
    {
        std::cout << "instruction -> TOKEN_SKIP TOKEN_SEMICOLON" << std::endl;
    }
|
    assignment
    {
        std::cout << "instruction -> assignment" << std::endl;
    }
|
    read
    {
        std::cout << "instruction -> read" << std::endl;
    }
|
    write
    {
        std::cout << "instruction -> write" << std::endl;
    }
|
    branch
    {
        std::cout << "instruction -> branch" << std::endl;
    }
|
    loop
    {
        std::cout << "instruction -> loop" << std::endl;
    }
;

assignment:
    TOKEN_ID TOKEN_ASSIGN expression TOKEN_SEMICOLON
    {
        std::cout << "assignment -> TOKEN_ID TOKEN_ASSIGN expression TOKEN_SEMICOLON" << std::endl;
    }
;

read:
    TOKEN_READ TOKEN_OPEN TOKEN_ID TOKEN_CLOSE TOKEN_SEMICOLON
    {
        std::cout << "read -> TOKEN_READ TOKEN_OPEN TOKEN_ID TOKEN_CLOSE TOKEN_SEMICOLON" << std::endl;
    }
;

write:
    TOKEN_WRITE TOKEN_OPEN expression TOKEN_CLOSE TOKEN_SEMICOLON
    {
        std::cout << "write -> TOKEN_WRITE TOKEN_OPEN expression TOKEN_CLOSE TOKEN_SEMICOLON" << std::endl;
    }
;

branch:
    TOKEN_IF expression TOKEN_THEN instructions TOKEN_ENDIF
    {
        std::cout << "branch -> TOKEN_IF expression TOKEN_THEN instructions TOKEN_ENDIF" << std::endl;
    }
|
    TOKEN_IF expression TOKEN_THEN instructions TOKEN_ELSE instructions TOKEN_ENDIF
    {
        std::cout << "branch -> TOKEN_IF expression TOKEN_THEN instructions TOKEN_ELSE instructions TOKEN_ENDIF" << std::endl;
    }
;

loop:
    TOKEN_WHILE expression TOKEN_DO instructions TOKEN_DONE
    {
        std::cout << "loop -> TOKEN_WHILE expression TOKEN_DO instructions TOKEN_DONE" << std::endl;
    }
;

expression:
    TOKEN_NUM
    {
        std::cout << "expression -> TOKEN_NUM" << std::endl;
    }
|
    TOKEN_TRUE
    {
        std::cout << "expression -> TOKEN_TRUE" << std::endl;
    }
|
    TOKEN_FALSE
    {
        std::cout << "expression -> TOKEN_FALSE" << std::endl;
    }
|
    TOKEN_ID
    {
        std::cout << "expression -> TOKEN_ID" << std::endl;
    }
|
    expression TOKEN_ADD expression
    {
        std::cout << "expression -> expression TOKEN_ADD expression" << std::endl;
    }
|
    expression TOKEN_SUB expression
    {
        std::cout << "expression -> expression TOKEN_SUB expression" << std::endl;
    }
|
    expression TOKEN_MUL expression
    {
        std::cout << "expression -> expression TOKEN_MUL expression" << std::endl;
    }
|
    expression TOKEN_DIV expression
    {
        std::cout << "expression -> expression TOKEN_DIV expression" << std::endl;
    }
|
    expression TOKEN_MOD expression
    {
        std::cout << "expression -> expression TOKEN_MOD expression" << std::endl;
    }
|
    expression TOKEN_LESS expression
    {
        std::cout << "expression -> expression TOKEN_LESS expression" << std::endl;
    }
|
    expression TOKEN_GR expression
    {
        std::cout << "expression -> expression TOKEN_GR expression" << std::endl;
    }
|
    expression TOKEN_EQ expression
    {
        std::cout << "expression -> expression TOKEN_EQ expression" << std::endl;
    }
|
    expression TOKEN_AND expression
    {
        std::cout << "expression -> expression TOKEN_AND expression" << std::endl;
    }
|
    expression TOKEN_OR expression
    {
        std::cout << "expression -> expression TOKEN_OR expression" << std::endl;
    }
|
    TOKEN_NOT expression
    {
        std::cout << "expression -> TOKEN_NOT expression" << std::endl;
    }
|
    TOKEN_OPEN expression TOKEN_CLOSE
    {
        std::cout << "expression -> TOKEN_OPEN expression TOKEN_CLOSE" << std::endl;
    }
;
